const test = require('tape');
const unified = require('unified');
const remarkParse = require('remark-parse');
const inspect = require('unist-util-inspect');
const linkifyRegex = require('./index');

test('it creates links matching a regex', (t) => {
  t.plan(2);

  const markdown = `
# Title

(This is my friend: @6ilZq3kN0F)

This is redundantly linked: [@6ilZq3kN0F](@6ilZq3kN0F)

cc [@alreadyLinked](@2RNGJafZt)
`;

  const actualInput = unified()
    .use(remarkParse, {commonmark: true})
    .parse(markdown);

  // console.log(JSON.stringify(actualInput));
  console.log(inspect(actualInput));

  const expectedInput = {
    type: 'root',
    position: {
      start: {line: 1, column: 1, offset: 0},
      end: {line: 9, column: 1, offset: 132},
    },
    children: [
      {
        type: 'heading',
        depth: 1,
        position: {
          start: {line: 2, column: 1, offset: 1},
          end: {line: 2, column: 8, offset: 8},
          indent: [],
        },
        children: [
          {
            type: 'text',
            value: 'Title',
            position: {
              start: {line: 2, column: 3, offset: 3},
              end: {line: 2, column: 8, offset: 8},
              indent: [],
            },
          },
        ],
      },
      {
        type: 'paragraph',
        position: {
          start: {line: 4, column: 1, offset: 10},
          end: {line: 4, column: 33, offset: 42},
          indent: [],
        },
        children: [
          {
            type: 'text',
            value: '(This is my friend: @6ilZq3kN0F)',
            position: {
              start: {line: 4, column: 1, offset: 10},
              end: {line: 4, column: 33, offset: 42},
              indent: [],
            },
          },
        ],
      },
      {
        type: 'paragraph',
        position: {
          start: {line: 6, column: 1, offset: 44},
          end: {line: 6, column: 55, offset: 98},
          indent: [],
        },
        children: [
          {
            type: 'text',
            value: 'This is redundantly linked: ',
            position: {
              start: {line: 6, column: 1, offset: 44},
              end: {line: 6, column: 29, offset: 72},
              indent: [],
            },
          },
          {
            type: 'link',
            title: null,
            url: '@6ilZq3kN0F',
            position: {
              start: {line: 6, column: 29, offset: 72},
              end: {line: 6, column: 55, offset: 98},
              indent: [],
            },
            children: [
              {
                type: 'text',
                value: '@6ilZq3kN0F',
                position: {
                  start: {line: 6, column: 30, offset: 73},
                  end: {line: 6, column: 41, offset: 84},
                  indent: [],
                },
              },
            ],
          },
        ],
      },
      {
        type: 'paragraph',
        position: {
          start: {line: 8, column: 1, offset: 100},
          end: {line: 8, column: 32, offset: 131},
          indent: [],
        },
        children: [
          {
            type: 'text',
            value: 'cc ',
            position: {
              start: {line: 8, column: 1, offset: 100},
              end: {line: 8, column: 4, offset: 103},
              indent: [],
            },
          },
          {
            type: 'link',
            title: null,
            url: '@2RNGJafZt',
            position: {
              start: {line: 8, column: 4, offset: 103},
              end: {line: 8, column: 32, offset: 131},
              indent: [],
            },
            children: [
              {
                type: 'text',
                value: '@alreadyLinked',
                position: {
                  start: {line: 8, column: 5, offset: 104},
                  end: {line: 8, column: 19, offset: 118},
                  indent: [],
                },
              },
            ],
          },
        ],
      },
    ],
  };
  t.deepEquals(actualInput, expectedInput, 'input looks good');

  const actualOutput = linkifyRegex(/\@[A-Za-z0-9]+\b/)()(actualInput);

  // console.log(JSON.stringify(actualOutput));
  console.log(inspect(actualOutput));

  const expectedOutput = {
    type: 'root',
    position: {
      start: {line: 1, column: 1, offset: 0},
      end: {line: 9, column: 1, offset: 132},
    },
    children: [
      {
        type: 'heading',
        depth: 1,
        position: {
          start: {line: 2, column: 1, offset: 1},
          end: {line: 2, column: 8, offset: 8},
          indent: [],
        },
        children: [{type: 'text', value: 'Title'}],
      },
      {
        type: 'paragraph',
        position: {
          start: {line: 4, column: 1, offset: 10},
          end: {line: 4, column: 33, offset: 42},
          indent: [],
        },
        children: [
          {type: 'text', value: '(This is my friend: '},
          {
            type: 'link',
            title: '@6ilZq3kN0F',
            url: '@6ilZq3kN0F',
            children: [{type: 'text', value: '@6ilZq3kN0F'}],
          },
          {type: 'text', value: ')'},
        ],
      },
      {
        type: 'paragraph',
        position: {
          start: {line: 6, column: 1, offset: 44},
          end: {line: 6, column: 55, offset: 98},
          indent: [],
        },
        children: [
          {type: 'text', value: 'This is redundantly linked: '},
          {
            type: 'link',
            title: null,
            url: '@6ilZq3kN0F',
            position: {
              start: {line: 6, column: 29, offset: 72},
              end: {line: 6, column: 55, offset: 98},
              indent: [],
            },
            children: [
              {
                type: 'text',
                value: '@6ilZq3kN0F',
                position: {
                  start: {line: 6, column: 30, offset: 73},
                  end: {line: 6, column: 41, offset: 84},
                  indent: [],
                },
              },
            ],
          },
        ],
      },
      {
        type: 'paragraph',
        position: {
          start: {line: 8, column: 1, offset: 100},
          end: {line: 8, column: 32, offset: 131},
          indent: [],
        },
        children: [
          {type: 'text', value: 'cc '},
          {
            type: 'link',
            title: null,
            url: '@2RNGJafZt',
            position: {
              start: {line: 8, column: 4, offset: 103},
              end: {line: 8, column: 32, offset: 131},
              indent: [],
            },
            children: [
              {
                type: 'text',
                value: '@alreadyLinked',
                position: {
                  start: {line: 8, column: 5, offset: 104},
                  end: {line: 8, column: 19, offset: 118},
                  indent: [],
                },
              },
            ],
          },
        ],
      },
    ],
  };

  t.deepEquals(actualOutput, expectedOutput, 'output looks good');
});

test('it supports unicode matching', (t) => {
  t.plan(2);

  const markdown = `
# Title

@árduo
`;

  const actualInput = unified()
    .use(remarkParse, {commonmark: true})
    .parse(markdown);

  // console.log(JSON.stringify(actualInput));
  console.log(inspect(actualInput));

  const expectedInput = {
    type: 'root',
    children: [
      {
        type: 'heading',
        depth: 1,
        children: [
          {
            type: 'text',
            value: 'Title',
            position: {
              start: {line: 2, column: 3, offset: 3},
              end: {line: 2, column: 8, offset: 8},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 2, column: 1, offset: 1},
          end: {line: 2, column: 8, offset: 8},
          indent: [],
        },
      },
      {
        type: 'paragraph',
        children: [
          {
            type: 'text',
            value: '@árduo',
            position: {
              start: {line: 4, column: 1, offset: 10},
              end: {line: 4, column: 7, offset: 16},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 4, column: 1, offset: 10},
          end: {line: 4, column: 7, offset: 16},
          indent: [],
        },
      },
    ],
    position: {
      start: {line: 1, column: 1, offset: 0},
      end: {line: 5, column: 1, offset: 17},
    },
  };
  t.deepEquals(actualInput, expectedInput, 'input looks good');

  const actualOutput = linkifyRegex(/@\p{Letter}+\b/u)()(actualInput);

  // console.log(JSON.stringify(actualOutput));
  console.log(inspect(actualOutput));

  const expectedOutput = {
    type: 'root',
    children: [
      {
        type: 'heading',
        depth: 1,
        children: [
          {
            type: 'text',
            value: 'Title',
          },
        ],
        position: {
          start: {
            line: 2,
            column: 1,
            offset: 1,
          },
          end: {
            line: 2,
            column: 8,
            offset: 8,
          },
          indent: [],
        },
      },
      {
        type: 'paragraph',
        children: [
          {
            type: 'link',
            title: '@árduo',
            url: '@árduo',
            children: [
              {
                type: 'text',
                value: '@árduo',
              },
            ],
          },
        ],
        position: {
          start: {
            line: 4,
            column: 1,
            offset: 10,
          },
          end: {
            line: 4,
            column: 7,
            offset: 16,
          },
          indent: [],
        },
      },
    ],
    position: {
      start: {
        line: 1,
        column: 1,
        offset: 0,
      },
      end: {
        line: 5,
        column: 1,
        offset: 17,
      },
    },
  };

  t.deepEquals(actualOutput, expectedOutput, 'output looks good');
});
