const visitWithParents = require('unist-util-visit-parents');
const flatMap = require('unist-util-flatmap');

function mergeFlags(...allFlags) {
  const set = new Set();
  for (const flags of allFlags) {
    for (const char of flags) {
      set.add(char);
    }
  }
  return [...set.values()].join('');
}

function removeExtremes(regex) {
  return new RegExp(
    regex.source.replace(/^\^/, '').replace(/\$$/, ''),
    mergeFlags('g', regex.flags),
  );
}

function buildTextNode(props) {
  return {type: 'text', value: props.value};
}

function buildLinkNode(props, children) {
  return {
    type: 'link',
    title: props.title || props.url,
    url: props.url,
    children,
  };
}

function h(type, props, children) {
  if (type === 'text') return buildTextNode(props, children);
  if (type === 'link') return buildLinkNode(props, children);
  throw new Error('mdast hyperscript not supported for type ' + type);
}

function splitTextNode(textNode, inputRegex) {
  const oldText = textNode.value;
  const regex = removeExtremes(inputRegex)
  const newNodes = [];
  let startTextIdx = 0;
  let output;
  while ((output = regex.exec(oldText)) !== null) {
    const endTextIdx = output.index;
    if (startTextIdx !== endTextIdx) {
      newNodes.push(
        h('text', {value: oldText.slice(startTextIdx, endTextIdx)}),
      );
    }
    const feedId = output[0];
    newNodes.push(h('link', {url: feedId}, [h('text', {value: feedId})]));
    startTextIdx = regex.lastIndex;
  }
  const remainingText = oldText.slice(startTextIdx);
  if (remainingText.length > 0) {
    newNodes.push(h('text', {value: remainingText}));
  }
  return newNodes;
}

function linkifyRegex(regex) {
  return () => (ast) => {
    const ignored = new WeakSet();
    visitWithParents(ast, 'text', (textNode, parents) => {
      if (parents.some(parent => parent.type === 'link')) {
        ignored.add(textNode);
        return;
      }
    });

    flatMap(ast, (node) => {
      if (node.type !== 'text') {
        return [node];
      }
      if (ignored.has(node)) {
        ignored.delete(node);
        return [node];
      }
      return splitTextNode(node, regex);
    });

    return ast;
  };
}

module.exports = linkifyRegex;
